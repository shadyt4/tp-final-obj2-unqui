package stackOverflow;

import insignia.InsigniaLogro;

import java.util.ArrayList;
import java.util.List;

import busqueda.Busqueda;
import post.Pregunta;
import usuario.UsuarioModerador;
import usuario.UsuarioSimple;
import usuario.UsuarioVisitante;
import criterio.Criterio;

public class StackOverflow {

	
	List<Pregunta> listaDePreguntas;
	List<UsuarioSimple> listaDeUsuariosSimple;
	List<UsuarioModerador> listaDeUsuariosModeradores;
	List<UsuarioVisitante> listaDeUsuariosVisitante;
	List<InsigniaLogro> listaInsignias;
	
	// Constructor, Inicializa todas las variables de instancia.
	public StackOverflow(){
		
		listaDePreguntas = new ArrayList<Pregunta>();
		listaDeUsuariosSimple  = new ArrayList<UsuarioSimple>();
		listaDeUsuariosModeradores  = new ArrayList<UsuarioModerador>();
		listaDeUsuariosVisitante  = new ArrayList<UsuarioVisitante>();
		listaInsignias = new ArrayList<InsigniaLogro>();
	}  
	
	// M�todo buscar(lista de Criterios), se encarga de buscar todas las preguntas 
	// que cumplan con la condici�n de todos los criterios.
	public List<Pregunta> buscar (List<Criterio> listaCriterios){
		
		Busqueda busqueda = new Busqueda(listaCriterios);
		
		return busqueda.realizarBusqueda(this.listaDePreguntas);
	}
	// agregarUsuarioModerador(String nombre, String email), agrega un usuario moderador a la lista 
	// de usuarios moderadores.
	public void agregarUsuarioModerador(String nombre, String email){
		
		UsuarioModerador usuarioModerador = new UsuarioModerador(nombre,email); 
		this.listaDeUsuariosModeradores.add(usuarioModerador);
	}
	// agregarUsuarioSimple(String nombre, String email, String NDU), agrega un usuario simple a la lista 
	// de usuarios simples.
	public void agregarUsuarioSimple(String nombre, String email, String NDU){
		
		UsuarioSimple usuarioSimple = new UsuarioSimple(nombre,email,NDU); 
		this.listaDeUsuariosSimple.add(usuarioSimple);
	}
	// agregarUsuarioVisitante(int IP), agrega un usuario visitante a la lista 
	// de usuarios visitantes.
	public void agregarUsuarioVisitante(int IP){
	
		UsuarioVisitante usuarioVisitante = new UsuarioVisitante(IP);
	this.listaDeUsuariosVisitante.add(usuarioVisitante);
	}
	//getPreguntas(), retorna la lista de preguntas.
	public List<Pregunta> getPreguntas(){
		return this.listaDePreguntas;
	}
	//getUsuariosSimples(), retorna la lista de usuarios simples.
	public List<UsuarioSimple> getUsuariosSimples(){
		return this.listaDeUsuariosSimple;
	}
	//getUsuariosModeradores(), retorna la lista de usuarios moderadores.
	public List<UsuarioModerador> getUsuariosModeradores(){
		return this.listaDeUsuariosModeradores;
	}
	//getUsuariosVisitantes(), retorna la lista de usuarios visitantes.
	public List<UsuarioVisitante> getUsuariosVisitantes(){
		return this.listaDeUsuariosVisitante;
	}
	// buscarUsuarioSimple(String nombre), con el String nombre que se le pasa por par�metro
	// busca al usuario y lo devuelve, si no lo encuentra devuelve null.
	public UsuarioSimple buscarUsuarioSimple(String nombre){
		
		return this.buscarYRetornarUsuarioSimple(nombre);	
	}
	// buscarUsuarioSimple(String nombre), con el String nombre que se le pasa por par�metro
	// busca al usuario y lo devuelve, si no lo encuentra devuelve null.
	public UsuarioSimple buscarYRetornarUsuarioSimple(String nombre) {// Abstraer
		
		boolean esta = false;
		UsuarioSimple usuarioS  = null;
		
		for(UsuarioSimple each: this.listaDeUsuariosSimple){
			esta  = esta || (each.getNombre()  == nombre);
			if(esta){
			usuarioS = each;
			}
		}
		return usuarioS;
	}
	// buscarUsuarioModerador(String nombre), con el String nombre que se le pasa por par�metro
	// busca al usuario y lo devuelve, si no lo encuentra devuelve null.
	public UsuarioModerador buscarUsuarioModerador(String nombre){
		
		return this. buscarYRetornarUsuarioModerador(nombre);
		
	}
	// buscarYRetornarUsuarioModerador(String nombre), con el String nombre que se le pasa por par�metro
	// busca al usuario y lo devuelve, si no lo encuentra devuelve null.
	public UsuarioModerador  buscarYRetornarUsuarioModerador(String nombre){// Abstraer
		
		boolean esta = false;
		UsuarioModerador usuarioM  = null;
		
		for(UsuarioModerador each: this.listaDeUsuariosModeradores){
			esta  = esta || (each.getNombre()  == nombre);
			if(esta){
			usuarioM = each;
			}
		}
		return usuarioM;
	}
	// buscarUsuarioVisitante(int IP), con el String nombre que se le pasa por par�metro
	// busca al usuario y lo devuelve, si no lo encuentra devuelve null.
	public UsuarioVisitante buscarUsuarioVisitante(int IP){
		
		return this. buscarYRetornarUsuarioVisitante(IP);
		
	}
	// buscarYRetornarUsuarioVisitante(int IP), con el String nombre que se le pasa por par�metro
	// busca al usuario y lo devuelve, si no lo encuentra devuelve null.
	public UsuarioVisitante  buscarYRetornarUsuarioVisitante(int IP){// Abstraer
		
		boolean esta = false;
		UsuarioVisitante usuarioV  = null;
		
		for(UsuarioVisitante each: this.listaDeUsuariosVisitante){
			esta  = esta || (each.getDireccionIP()  == IP);
			if(esta){
			usuarioV = each;
			}
		}
		return usuarioV;
	}

	/*
	 * Agrega la pregunta del parametro a la listaDePreguntas del Stack.
	 */
	public void agregarPregunta(Pregunta pregunta) {
		this.getPreguntas().add(pregunta);
	}
	
	public List<InsigniaLogro> getInsignias() {
		
		return this.listaInsignias;
	}
	
	
}