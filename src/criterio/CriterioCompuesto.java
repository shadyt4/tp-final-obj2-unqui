package criterio;

import java.util.ArrayList;

import post.Pregunta;

public class CriterioCompuesto extends Criterio{

	ArrayList<Criterio> listaDeCriterios;
	// Constructor.
	public CriterioCompuesto(){
		this.listaDeCriterios = new ArrayList<Criterio>();
		
	}
	// Constructor, se le pasa una lista de criterios por parámetro.
	public CriterioCompuesto(ArrayList<Criterio> listaDeCriterios){
		this.listaDeCriterios = listaDeCriterios;
		
	}
	// agregarCriterio(Criterio criterio), agrega un criterio que se le pasa por parámetro 
	// a la variable de instancia "listaDeCriterios".
	public void agregarCriterio(Criterio criterio){
		
		this.listaDeCriterios.add(criterio);
	}
	// getListaDeCriterios(), devuelve la lista de Criterios(variable de instancia).
	public ArrayList<Criterio> getListaDeCriterios(){
		
		return this.listaDeCriterios;
	}
	// setListaDeCriterios(ArrayList<Criterio> listaDeCriterios), se le pasa una listaDeCriterios por parámetro y setea la V.I.
	public void setListaDeCriterios(ArrayList<Criterio> listaDeCriterios){
		
		this.listaDeCriterios = listaDeCriterios;
	}
	// getCantidadDeCriterios(), devuelve un int, que indica el largo de la listaDeCriterios(variable de instancia).
	public int getCantidadDeCriterios(){
		return this.getListaDeCriterios().size();
	}
	// cumpleCondicion(Pregunta p), devuelve un booleano indicando si cada criterio de la listaDeCriterios(V.I), 
	// cumple condición con la pregunta.
	@Override
	public boolean cumpleCondicion(Pregunta p) {
		
		boolean cumpleCondicion = true;
		for(Criterio each: this.listaDeCriterios){
			
			cumpleCondicion = cumpleCondicion && each.cumpleCondicion(p);
			
		}
		
		return cumpleCondicion;
	}
	

}
