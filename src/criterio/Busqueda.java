package criterio;

import java.util.ArrayList;
import java.util.List;

import post.Pregunta;

public class Busqueda {

	List<Criterio> listaDeCriterios;
	
	public Busqueda(List<Criterio> listaDeCriterios){
		
		this.listaDeCriterios = listaDeCriterios;
	}
	
  public List<Pregunta> realizarBusqueda(List<Pregunta> listaDePreguntas){
		
		List<Pregunta> listaDePreguntasCumpleCondicion = new ArrayList<Pregunta>();
		
		boolean cumpleCondicion = false;
		
		for(Pregunta pregunta: listaDePreguntas){
			for(Criterio criterio: listaDeCriterios){
					cumpleCondicion = cumpleCondicion || criterio.cumpleCondicion(pregunta);
			}
			
			if(cumpleCondicion){
				listaDePreguntasCumpleCondicion.add(pregunta);
			}
			
		}
		return listaDePreguntasCumpleCondicion;
	}

}
