package criterio;

import post.Pregunta;

public class Autor extends Criterio{

	String contenido;
	// Contructor, se le pasa por parámetro un String.
	public Autor(String autor){
		
		this.contenido = autor;
	}
	//getContenido(), devuelve la V.I.
	public String getContenido(){
		return this.contenido;
	}
	// cumpleCondicion(Pregunta p), devuelve un booleano indicando 
	// si la V.I es igual a el autor de la pregunta que se pasa como parámetro.
	@Override
	public boolean cumpleCondicion(Pregunta p) {
		return p.dameNombreDeAutor() == this.getContenido();
	}

}
