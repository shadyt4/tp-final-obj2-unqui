package criterio;

import org.joda.time.DateTime;

import post.Pregunta;

public class FechaDeRealizacion extends Criterio{

	DateTime contenido;
	
	// Contructor, se le pasa por parámetro una fecha.
	public FechaDeRealizacion(DateTime fecha){
		
		this.contenido = fecha;
	}
	// getContenido(), devuelve la variable de instancia que es una fecha.
	public DateTime getContenido(){
		
		return contenido;
	}
	// cumpleCondicion(Pregunta p), devuelve un booleano indicando 
	// si la variable de instancia es igual a la fecha de la pregunta.
	@Override
	public boolean cumpleCondicion(Pregunta p) {
		return p.getFechaDePublicacion() == this.contenido;
	}
	// setContenido(DateTime fecha), se le pasa una fecha como parámetro y la modifica por la variable de instancia.
	public void setContenido(DateTime fecha){
		this.contenido = fecha;
	}
	
}
