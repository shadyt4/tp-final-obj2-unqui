package criterio;

import post.Pregunta;

public class Titulo extends Criterio{

    String contenido;
    
    // Contructor con parámetro (String contenido).
    public Titulo(String contenido){
        this.contenido = contenido;
    }

    // getContenido(), devuelve la variable de instancia String contenido.
    public String getContenido() {
        // TODO Auto-generated method stub
        return this.contenido;
    }

    // cumpleCondicion(Pregunta p), retorna un booleano indicando si la variable de instancia es igual 
    // al titulo de la pregunta que se pasa como parámetro.
	@Override
	public boolean cumpleCondicion(Pregunta p) {
		// TODO Auto-generated method stub
		return (this.getContenido() == p.getTitulo());
	}
}