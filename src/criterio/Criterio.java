package criterio;

import post.Pregunta;

public abstract class Criterio {
    
    public Criterio() {
        super();
    }
    // clase abstracta cumpleCondicion(Pregunta p), que la implementa cada subclase.
    public abstract boolean cumpleCondicion(Pregunta p);

}