package criterio;

import post.Pregunta;

public class Etiqueta extends Criterio{

    String contenido;
    // Contructor, se pasa como parámetro un String etiqueta.
    public Etiqueta(String etiqueta){
        
        this.contenido = etiqueta;
    }
    // getContenido(), devuelve la variable de instancia.
    public String getContenido() {
        // TODO Auto-generated method stub
        return this.contenido;
    }
    
    // cumpleCondicion(Pregunta p), verifica si la variable de instancia contenido es igual a 
    // alguna de las etiquetas que contiene la pregunta.
	@Override
	public boolean cumpleCondicion(Pregunta p) {
		
		boolean coincide = false;
		
		for(String each: p.getEtiquetas()){
			
			coincide =  (each == this.contenido);
		}
		return coincide;
	}
}