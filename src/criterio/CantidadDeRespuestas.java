package criterio;

import post.Pregunta;

public class CantidadDeRespuestas extends Criterio{

    int contenido;
    // Contructor, se le pasa un int como parámetro.
    public CantidadDeRespuestas(int contenido){
        this.contenido = contenido;
    }
    // getContenido(), devuelve la V.I
    public int getContenido() {
        // TODO Auto-generated method stub
        return this.contenido;
    }
    // cumpleCondicion(Pregunta pregunta), devuelve un booleano indicando 
    // si la V.I es igual a la cantidad de respuestas de la pregunta que se pasa como parámetro.
    public boolean cumpleCondicion(Pregunta pregunta) {
        
        return this.contenido == pregunta.getRespuestas().size();
    }
}
