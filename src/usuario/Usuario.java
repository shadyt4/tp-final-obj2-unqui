package usuario;

import stackOverflow.StackOverflow;

/*
 * Esta clase es la superior jerarquicamente dentro de los usuarios,
 * es abstracta ya que solo tiene variables y metodos compartidas entre las tres categorias de
 * usuarios importantes: UsuarioSimple, UsuarioModerador y UsuarioVisitante.
 */

public abstract class Usuario {

	/*
	 * Inicializamos las variables en 0(cero) porque los usuarios al ser creados
	 * no tienen ningun valor en estas. Luego son modificadas en el metodo calcularRanking().
	 */
	int ranking = 0;
	int puntajePorActividad = 0;
	float factorDeRegularidad = 0;
	int cantidadDeRespuestas = 0;
	StackOverflow stackOverflow;
	
	/*
	 *  Metodos abstractos, son implementados de acuerdo a cada usuario.
	 */
	public abstract float calcularFactorDeRegularidad();
	public abstract int calcularPuntajePorActividad();
	
	/* 
	 * calcularRanking() primero calcula y guarda los valores en sus variables, 
	 * respectivamente(puntajePorActividad, factorDeRegularidad) para despues hacer
	 * el calculo requerido para saber el ranking del usuario.
	 */
	public float calcularRanking(){
		this.puntajePorActividad = this.calcularPuntajePorActividad();
		this.factorDeRegularidad = this.calcularFactorDeRegularidad();
		
		return this.puntajePorActividad * this.factorDeRegularidad;
	}
	
	public int getCantidadDeRespuestas(){
		return this.cantidadDeRespuestas;
	}
	
	public StackOverflow getStackOverflow() {
		return this.stackOverflow;
	}
	
	public void setStackOverflow(StackOverflow stack) {
		this.stackOverflow = stack;
	}
}
