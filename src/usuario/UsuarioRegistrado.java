package usuario;

import java.util.Date;

/*
 * Clase abstracta de la cual heredan las subclases: UsuarioSimple y UsuarioModerador.
 */
public abstract class UsuarioRegistrado extends Usuario{

	/*
	 * Variables compartidas por los usuarios herederos.
	 */
	String nombre;
	String email;
	Date fechaDeRegistracion;
	int cantidadDePreguntas = 0;
	
	public UsuarioRegistrado(String n, String e){
		this.nombre = n;
		this.email = e;
		this.fechaDeRegistracion = new Date();
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public String getEmail() {
		return this.email;
	}

	public Date getFechaDeRegistracion() {
		return this.fechaDeRegistracion;
	}
	
	public int getCantidadDePreguntas() {
		return this.cantidadDePreguntas;
	}
	
	/*
	 * El valor asignado por respuesta realizada es de 5 , mientras que por pregunta es de 10.
	 */
	@Override
	public int calcularPuntajePorActividad() {
		return (10* this.cantidadDePreguntas) + (5* this.cantidadDeRespuestas);
	}
	
	/*
	 * Metodos abstractos que de acuerdo a que usuario sea son implementadas diferentes,
	 * son usados por calcularFactorDeRegularidad() para saber si el usuario es regular,
	 * tanto haciendo/modificando preguntas y respuestas.
	 */
	public abstract boolean esRegularRespondiendo();
	public abstract boolean esRegularPreguntando();
	
	/*
	 * Metodo que devuelve 1 si el usuario es regular y 0.75 si no lo es.
	 */
	@Override
	public float calcularFactorDeRegularidad() {
		
		if(this.esRegularPreguntando() | this.esRegularRespondiendo()){
			return 1;
		}
		
		return 0.75f;
	}
}
