package usuario;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;

import post.Pregunta;
import post.Respuesta;

/*
 * Esta clase de usuario hereda directamente desde Usuario,
 * solo contiene la variable de direccionIP y las preguntas que respondio.
 */
public class UsuarioVisitante extends Usuario{
	
	int direccionIP = 0;
	List<Pregunta> preguntasRespondidas = new ArrayList<Pregunta>();

	
	public UsuarioVisitante(int IP){
		this.direccionIP = IP;
	}
	
	public int getDireccionIP() {
		return this.direccionIP;
	}

	public List<Pregunta> getPreguntasRespondidas() {
		return this.preguntasRespondidas;
	}
	
	/*
	 * Este metodo recibe la pregunta la cual se va a responder y el texto de la respuesta,
	 * despues es guardada en la lista de preguntas respondidas del usuario.
	 */
	public void realizarRespuesta(Pregunta pregunta, String cuerpo) {
		pregunta.responder(cuerpo, this);
		this.preguntasRespondidas.add(pregunta);
		this.cantidadDeRespuestas++;
	}

	/*
	 * Implementa este metodo diferente a los otros usuarios ya que solamente
	 * este usuario puede responder preguntas, entonces es regular solo respondiendo.
	 */
	@Override
	public float calcularFactorDeRegularidad() {
		if(esRegularRespondiendo()){
			return 1;
		}
		return 0.75f;
	}
	
	public boolean esRegularRespondiendo() {
		int mesAhora = DateTime.now().getMonthOfYear();
		
		for(Pregunta p : this.getPreguntasRespondidas()){
			for(Respuesta r : p.getRespuestas()){
				if( r.getAutor().equals(this) ){
					int mesDeLaRespuesta = r.getFechaDePublicacion().getMonthOfYear();
					int diferencia = mesAhora- mesDeLaRespuesta;
					if( ((0 <= diferencia) && (diferencia <= 6)) | (diferencia <= -6) ){ return true; }
				}
			}
		}
		
		return false;
	}

	/*
	 * El valor asignado por respuesta realizada es de 2.
	 */
	@Override
	public int calcularPuntajePorActividad() {
		return 2* this.cantidadDeRespuestas;
	}
	
}
