package usuario;

import insignia.InsigniaLogro;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;

import post.Pregunta;
import post.Respuesta;

/*
 * En la clase UsuarioSimple se agrega la variable nombreDeUsuario, y las listas
 * pasan a ser preguntasRespondidas y preguntasPublicadas.
 */
public class UsuarioSimple extends UsuarioRegistrado {

	String nombreDeUsuario;
	List<Pregunta> preguntasRespondidas = new ArrayList<Pregunta>();
	List<Pregunta> preguntasPublicadas = new ArrayList<Pregunta>();
	
	public UsuarioSimple(String n, String e, String NDU) {
		super(n, e);
		this.nombreDeUsuario = NDU;
	}

	public String getNombreDeUsuario() {
		return this.nombreDeUsuario;
	}

	public List<Pregunta> getPreguntasPublicadas() {
		return this.preguntasPublicadas;
	}
	
	public List<Pregunta> getPreguntasRespondidas() {
		return this.preguntasRespondidas;
	}
	
	/*
	 * El metodo realizarPregunta() recibe el titulo y el cuerpo de la publicacion,
	 * luego de ya echa la pregunta se guarda en las preguntasPublicadas del usuario
	 * y en la lista del StackOverflow.
	 */
	public void realizarPregunta(String titulo, String cuerpo) {
		Pregunta pregunta = new Pregunta(titulo, cuerpo, this);
		this.getPreguntasPublicadas().add(pregunta);
		this.cantidadDePreguntas++;
		this.agregarPreguntaAlStackOverflow(pregunta);
		
	}

	private void agregarPreguntaAlStackOverflow(Pregunta pregunta) {
		this.getStackOverflow().agregarPregunta(pregunta);
	}

	/*
	 * Este metodo recibe la pregunta la cual se va a responder y el texto de la respuesta,
	 * despues es guardada en la lista de preguntas respondidas del usuario.
	 */
	public void realizarRespuesta(Pregunta pregunta, String cuerpo) {
		pregunta.responder(cuerpo, this);
		this.preguntasRespondidas.add(pregunta);
		this.cantidadDeRespuestas++;
	}

	/*
	 * Marca la pregunta del parametro como favorita del usuario.
	 */
	public void marcarComoFavorito(Pregunta pregunta) {
		pregunta.favoritoDe(this);
	}

	/*
	 * Le da un voto positivo a la pregunta. 
	 * Si la pregunta ya fue votada por el usuario no se suma el voto.
	 */
	public void votarPositivo(Pregunta pregunta) {
		pregunta.votarPositivo(this);
	}

	/*
	 * Le da un voto negativo a la pregunta.
	 * Si ya tenia un voto positivo del usuario le sacaria el voto a la pregunta.
	 */
	public void votarNegativo(Pregunta pregunta) {
		pregunta.votarNegativo(this);
		
	}

	public boolean esRegularRespondiendo() {
		int mesAhora = DateTime.now().getMonthOfYear();
		
		for(Pregunta p : this.getPreguntasRespondidas()){
			for(Respuesta r : p.getRespuestas()){
				if( r.getAutor().equals(this) ){
					int mesDeLaRespuesta = r.getFechaDePublicacion().getMonthOfYear();
					int diferencia = mesAhora- mesDeLaRespuesta;
					if( ((0 <= diferencia) && (diferencia <= 6)) | (diferencia <= -6) ){ return true; }
				}
			}
		}
		
		return false;
	}

	public boolean esRegularPreguntando() {
		int mesAhora = new DateTime().getMonthOfYear();
		
		for(Pregunta p : this.getPreguntasPublicadas()){
			
			int mesDeLaPregunta = p.getFechaDePublicacion().getMonthOfYear();
			int diferencia = mesAhora- mesDeLaPregunta;
			
			if( ((0 <= diferencia) && (diferencia <= 6)) | (diferencia <= -6) ){ return true; }
		}
		return false;
	}
	
	/*
	 * Borra la pregunta de la lista de publicadas.
	 */
	public void borrarPreguntaPublicada(Pregunta pregunta) {
		this.getPreguntasPublicadas().remove(pregunta);
		
	}
	
	/*
	 * Borra la pregunta de la lista de respondidas.
	 */
	public void borrarPreguntaRespondida(Pregunta pregunta) {
		this.getPreguntasRespondidas().remove(pregunta);
		
	}
	
	/*
	 * Borra la respuesta de una pregunta guardada en la lista de preguntas respondidas.
	 * Si esta pregunta tiene mas de una respuesta del autor solo borra la respuesta del parametro,
	 * ahora si solo tiene esa respuesta , entonces la pregunta es borrada de la lista de preguntas
	 * respondidas por el usuario.
	 */	
	public void borrarRespuesta(Respuesta respuesta) {
		for(Pregunta p : this.getPreguntasRespondidas()){
			if(p.contieneMasDeUnaRespuesta(respuesta.getAutor())){
				p.getRespuestas().remove(respuesta);
			}else{
				this.borrarPreguntaRespondida(p);
				break;
			}
		}
	}
	
	public ArrayList<InsigniaLogro> getInsigniasObtenidas() {
		ArrayList<InsigniaLogro> insigniasObtenidas = new ArrayList<InsigniaLogro>();
		
		for (InsigniaLogro insigniaLogro : this.stackOverflow.getInsignias()) {
			
			if(insigniaLogro.cumpleCondicion(this)){
				
				insigniasObtenidas.add(insigniaLogro);
			}
			
		}
		
		return insigniasObtenidas;
		
	}
	
	public ArrayList<InsigniaLogro> getInsigniasPorCumplir(){
		ArrayList<InsigniaLogro> insigniasPorCumplir = new ArrayList<InsigniaLogro>();
		
		for (InsigniaLogro insigniaLogro : this.stackOverflow.getInsignias()) {
			
			if(!insigniaLogro.cumpleCondicion(this)){
				
				insigniasPorCumplir.add(insigniaLogro);
			}
			
		}
		
		return insigniasPorCumplir;
	}
}
