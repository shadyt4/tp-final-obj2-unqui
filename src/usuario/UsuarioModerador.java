package usuario;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.joda.time.DateTime;

import post.Pregunta;
import post.Respuesta;

/*
 * De esta clase de usuario se conoce el nombre , email y fecha de registracion.
 * Estos pueden moderar preguntas y respuestas , modificarlas o borrarlas.
 */
public class UsuarioModerador extends UsuarioRegistrado {
	
	List<Pregunta> preguntasModeradas = new ArrayList<Pregunta>();
	List<Respuesta> respuestasModeradas = new ArrayList<Respuesta>();

	public UsuarioModerador(String n, String e) {
		super(n, e);
	}
	
	/*
	 * Este metodo recibe como String la parte de la pregunta que se quiere modificar y
	 * las modificaciones que realiza el moderador.
	 */
	public void modificarPregunta(Pregunta p, String parteAModificar, String modificacion, UsuarioModerador moderador) {
		if(parteAModificar == "Titulo"){
			p.setTitulo(modificacion);
			p.seModeroPor(moderador);
		}
		if(parteAModificar == "Cuerpo"){
			p.setCuerpo(modificacion);
			p.seModeroPor(moderador);
		}
		this.preguntasModeradas.add(p);
		this.cantidadDePreguntas++;
	
	}

	/*
	 * Este metodo recibe las modificaciones que realiza el moderador en la respuesta.
	 */
	public void modificarRespuesta(Respuesta r, String modificacion, UsuarioModerador uModerador) {
		r.setCuerpo(modificacion);
		r.seModeroPor(uModerador);
		this.respuestasModeradas.add(r);
		this.cantidadDeRespuestas++;
		
	}
	
	public List<Pregunta> getPreguntasModeradas() {
		return this.preguntasModeradas;
	}
	
	public List<Respuesta> getRespuestasModeradas() {
		return this.respuestasModeradas;
	}
	
	
	@Override
	public boolean esRegularRespondiendo() {
		int mesAhora = DateTime.now().getMonthOfYear();
		
		for(Respuesta r : this.getRespuestasModeradas()){
			Iterator<UsuarioModerador> it = r.getModeradores().keySet().iterator();
			while(it.hasNext()){
				UsuarioModerador uActual = it.next();
				if( uActual.equals(this) ){
					int mesDeLaModificacion = r.getModeradores().get(uActual).getMonthOfYear();
					int diferencia = mesAhora- mesDeLaModificacion;
					if( ((0 <= diferencia) && (diferencia <= 6)) | (diferencia <= -6) ){ return true; }
				}
			}
		}
		
		return false;
	}

	@Override
	public boolean esRegularPreguntando() {

		int mesAhora = new DateTime().getMonthOfYear();
		
		for(Pregunta p : this.getPreguntasModeradas()){
			Iterator<UsuarioModerador> it = p.getModeradores().keySet().iterator();
			while(it.hasNext()){
				UsuarioModerador uActual = it.next();
				
				int mesDeLaModificacion = p.getModeradores().get(uActual).getMonthOfYear();
				int diferencia = mesAhora- mesDeLaModificacion;
				if( ((0 <= diferencia) && (diferencia <= 6)) | (diferencia <= -6) ){ return true; }
			}
		}
		return false;
	}

	/*
	 * El metodo eliminar pregunta recibe la pregunta que se quiere eliminar.
	 */
	public void eliminarPregunta(Pregunta pregunta){
		
		UsuarioSimple usuarioAutor = (UsuarioSimple) pregunta.getAutor();
		usuarioAutor.borrarPreguntaPublicada(pregunta);
		
	}
	/*
	 * El metodo eliminar pregunta recibe la respuesta que se quiere eliminar.
	 */
	public void eliminarRespuesta(Respuesta respuesta){
		
		UsuarioSimple usuarioAutor = (UsuarioSimple) respuesta.getAutor();
		usuarioAutor.borrarRespuesta(respuesta);
		
	}
}
