package busqueda;

import java.util.ArrayList;
import java.util.List;

import criterio.Criterio;
import post.Pregunta;

public class Busqueda {

	List<Criterio> listaDeCriterios;
	
	// Constructor de la clase, que se le pasa por par�metro una lista de Criterios.
	public Busqueda(List<Criterio> listaCriterios){
		
		this.listaDeCriterios = listaCriterios;
	}
	// M�todo realizarPregunta(lista de Criterios), lo que hace es verificar que cada pregunta 
	// que se le pasa por par�metro cumpla con todos los criterios que fueron asignados al crearse la B�squeda,  
	// y retornar la lista de Preguntas.
public List<Pregunta> realizarBusqueda(List<Pregunta> listaDePreguntas){
		
		List<Pregunta> listaDePreguntasCumpleCondicion = new ArrayList<Pregunta>();
		boolean cumpleCondicion = true;
		
		for(Pregunta pregunta: listaDePreguntas){
			for(Criterio criterio: listaDeCriterios){
					cumpleCondicion = cumpleCondicion && criterio.cumpleCondicion(pregunta);
			}
			if(cumpleCondicion){
				listaDePreguntasCumpleCondicion.add(pregunta);
			}
			
		}
		return listaDePreguntasCumpleCondicion;
	}
}
