package insignia;

import usuario.*;

public abstract class InsigniaLogro {
	
	String nombre;
	UsuarioModerador autor;
	int contenido;
	
	public abstract boolean cumpleCondicion(UsuarioSimple usuSimple);
	public abstract String detalleDeInsignia(UsuarioSimple usuSimple);
	
	public String getNombre() {
		
		return this.nombre;
	}

}
