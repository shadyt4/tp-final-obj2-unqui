package insignia;

import java.util.ArrayList;

import usuario.UsuarioModerador;
import usuario.UsuarioSimple;

public class InsigniaCompuesta extends InsigniaLogro{

	ArrayList<InsigniaLogro> listaInsignias = new ArrayList<InsigniaLogro>();
	
	public InsigniaCompuesta(String nombre, UsuarioModerador usuModerador) {

		this.nombre = nombre;
		this.autor = usuModerador;
		this.contenido = 0;
	}
	
	public boolean cumpleCondicion(UsuarioSimple usuSimple){
		boolean result = true;
		for (InsigniaLogro insigniaLogro : listaInsignias) {
			result = result && insigniaLogro.cumpleCondicion(usuSimple);
		}
		return result;
	}

	public void agregarInsignia(InsigniaLogro insignia1) {
	
		this.listaInsignias.add(insignia1);
	}
	
	
	public String detalleDeInsignia(UsuarioSimple usuSimple){
		
		String result = "";
		
		for (InsigniaLogro insigniaLogro : listaInsignias) {
			
			result = result +insigniaLogro.detalleDeInsignia(usuSimple) +"// ";
		}
		
		return result;
		
	}
	
}
