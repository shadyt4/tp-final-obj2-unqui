package insignia;

import org.joda.time.DateTime;

import post.Pregunta;
import usuario.UsuarioModerador;
import usuario.UsuarioSimple;

public class MasDe5PreguntasEnUnaSemana extends InsigniaLogro{
	
	public MasDe5PreguntasEnUnaSemana(String nombre, UsuarioModerador usuModerador) {
		
		this.nombre = nombre;
		this.autor = usuModerador;
		this.contenido = 5;
	}
	
	public boolean cumpleCondicion(UsuarioSimple usuSimple) {
		int result = 0;
		
		for (Pregunta preg : usuSimple.getPreguntasPublicadas()) {
			if (preg.getFechaDePublicacion().isAfter((new DateTime()).minusDays(7)))
			{
				result ++;
			}
		}
		
		return result > contenido;
	}
	
	public String retornarMensajeDetalle(int cantPreguntasHechas) {
		String result = "Publico " +(cantPreguntasHechas) +" preguntas en los ultimos siente dias. Debe publicar " +(this.contenido-cantPreguntasHechas+1) +"preguntas m�s"; 
		return result;
	}
	
	public String detalleDeInsignia(UsuarioSimple usuSimple){
		
		int preguntasHechas = 0;
		
		for (Pregunta preg : usuSimple.getPreguntasPublicadas()) {
			if (preg.getFechaDePublicacion().isAfter((new DateTime()).minusDays(7)))
			{
				preguntasHechas ++;
			}
		}
		
		return this.retornarMensajeDetalle(preguntasHechas);
	}

}
