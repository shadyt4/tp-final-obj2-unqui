package insignia;

import usuario.UsuarioModerador;
import usuario.UsuarioSimple;

public class CantidadPreguntasMayorA50 extends InsigniaLogro{

	public CantidadPreguntasMayorA50(String nombre, UsuarioModerador usuModerador) {
		
		this.nombre = nombre;
		this.autor = usuModerador;
		this.contenido = 50;
	}
	
	public boolean cumpleCondicion(UsuarioSimple usuSimple){
		return usuSimple.getCantidadDePreguntas() > contenido;
	}

	
	public String retornarMensajeDetalle(int cantPreguntasHechas) {
		String result = "Realizo " +(cantPreguntasHechas) +" preguntas. Para obtener la isignia debera realizar " +(this.contenido-cantPreguntasHechas+1) +" preguntas m�s."; 
		return result;
	}
	
	public String detalleDeInsignia(UsuarioSimple usuSimple){
		
		return this.retornarMensajeDetalle(usuSimple.getCantidadDePreguntas());
	}
}
