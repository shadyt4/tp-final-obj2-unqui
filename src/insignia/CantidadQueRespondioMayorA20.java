package insignia;

import usuario.UsuarioModerador;
import usuario.UsuarioSimple;

public class CantidadQueRespondioMayorA20 extends InsigniaLogro{

	public CantidadQueRespondioMayorA20(String nombre,UsuarioModerador usuario) {
		
		this.nombre = nombre;
		this.autor = usuario;
		this.contenido = 20;
	}
	
	public boolean cumpleCondicion(UsuarioSimple usuSimple){
		return usuSimple.getCantidadDeRespuestas() > contenido;
	}

	public String retornarMensajeDetalle(int cantRespuestas) {
		String result = "Realizo " +(cantRespuestas) +" respuestas. Para obtener la isignia debera realizar " +(this.contenido-cantRespuestas+1) +" respuestas m�s.";
		return result;
	}
	
	public String detalleDeInsignia(UsuarioSimple usuSimple){
		
		return this.retornarMensajeDetalle(usuSimple.getCantidadDeRespuestas());
	}
}
