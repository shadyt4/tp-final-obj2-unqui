package post;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;

import usuario.*;

/*
 * La clase abstracta Post contiene las variables y metodos compartidos por
 * las clases Pregunta y Respuesta.
 */
public abstract class Post {

	String cuerpo;
	DateTime fechaDePublicacion;	
	DateTime ultimaModificacion;
	
	Map<UsuarioModerador, DateTime> moderadores = new HashMap<UsuarioModerador, DateTime>();
	
	Set<UsuarioRegistrado> favoritos = new HashSet<UsuarioRegistrado>();
	Map<UsuarioRegistrado, Boolean> votos = new HashMap<UsuarioRegistrado, Boolean>();
	

	@Override
	public abstract boolean equals(Object p);
	
	
	public void favoritoDe(UsuarioRegistrado usuario){
		this.favoritos.add(usuario);
	}
	
	public int cantidadDeFavoritos(){
		return this.favoritos.size();
	}

	public Set<UsuarioRegistrado> getFavoritos(){
		return this.favoritos;
	}

	public String getCuerpo() {
		return this.cuerpo;
	}

	public void setCuerpo(String cuerpo) {
		this.cuerpo = cuerpo;
	}
	
	/*
	 * El voto es guardado en un Map, clave usuario,mientras el valor es boolean
	 * (true si es positivo y false de lo contrario),
	 */
	public void votarPositivo(UsuarioRegistrado usuario){
		this.votos.put(usuario, true);
	}
	
	public void votarNegativo(UsuarioRegistrado usuario) {		
		this.votos.put(usuario, false);
	}

	public Map<UsuarioRegistrado, Boolean> getVotos() {
		return this.votos;
	}

	public DateTime getFechaDePublicacion() {
		return this.fechaDePublicacion;
	}

	public void seModifico() {
		this.ultimaModificacion = new DateTime();
	}
	
	public void seModeroPor(UsuarioModerador moderador) {
		this.seModifico();
		this.getModeradores().put(moderador, this.ultimaModificacion);
	}
	
	public int cantidadDeVotos(){
		int ret = 0;
		Iterator<UsuarioRegistrado> it = votos.keySet().iterator();
		while(it.hasNext()){
			UsuarioRegistrado uActual = it.next();
			if(votos.get(uActual) == true){ 
				ret++; 
			}
		}
		return ret;
	}
	
	public Map<UsuarioModerador, DateTime> getModeradores() {
		return this.moderadores;
	}

}
