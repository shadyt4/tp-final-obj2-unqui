package post;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;

import usuario.Usuario;
import usuario.UsuarioRegistrado;

public class Pregunta extends Post {
	
	UsuarioRegistrado autor;
	String titulo;
	List<Respuesta> respuestas = new ArrayList<Respuesta>();
	List<String> etiquetas = new ArrayList<String>();


	public Pregunta(String t, String c, UsuarioRegistrado usuario) {
		this.titulo = t;
		this.cuerpo = c;
		this.fechaDePublicacion = new DateTime();
		this.autor = usuario;
	}
	
	@Override
	public boolean equals(Object p) {
		return this.titulo == ((Pregunta) p).titulo & this.cuerpo == ((Pregunta) p).cuerpo 
				& this.fechaDePublicacion == ((Pregunta) p).fechaDePublicacion & this.autor == ((Pregunta) p).autor;
	}

	public void responder(String cuerpo, Usuario usuario) {
		Respuesta respuesta = new Respuesta(cuerpo, usuario);
		this.getRespuestas().add(respuesta);
	}
	
	public UsuarioRegistrado getAutor() {
		return this.autor;
	}

	public List<Respuesta> getRespuestas() {
		return this.respuestas;
	}

	public int getCantidadDeRespuestas() {
		return this.respuestas.size();
	}

	public String getTitulo() {
		return this.titulo;
	}
	
	public void setTitulo(String modificacion){
		this.titulo = modificacion;
	}
	
	public List<String> getEtiquetas() {
		return this.etiquetas;
	}
	
	public void addEtiqueta(String e){
		this.etiquetas.add(e);
	}
	
	public int getCantidadDeEtiquetas() {
		return this.getEtiquetas().size();
	}
	
	public String dameNombreDeAutor(){
		return this.getAutor().getNombre();
	}

	public boolean contieneMasDeUnaRespuesta(Usuario autor) {
		int cont = 0;
		for(Respuesta r : this.getRespuestas()){
			if( r.getAutor().equals(autor)){
				cont++;
			}
		}
		return cont > 1;
	}

}
