package post;

import org.joda.time.DateTime;

import usuario.Usuario;

public class Respuesta extends Post{
	
	Usuario autor;
	
	public Respuesta(String c) {
		this.cuerpo = c;
	}
	
	public Respuesta(String c, Usuario usuario) {
		this.cuerpo = c;
		this.autor = usuario;
		this.fechaDePublicacion = new DateTime();
	}
	
	public Usuario getAutor() {
		return this.autor;
	}

	@Override
	public boolean equals(Object r) {
		return this.cuerpo.equals(((Respuesta) r).cuerpo);
	}

}
