package insignia;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import usuario.*;

public class TestCantidadQueRespondioMayorA20{
	
	private UsuarioModerador usuModerador;
	private String nombre;
	private CantidadQueRespondioMayorA20 insignia;
	private UsuarioSimple usuSimple;
	
	@Before
	public void setUp(){
		
		usuModerador = Mockito.mock(UsuarioModerador.class);
		nombre = "nombreInsignia";
		insignia = new CantidadQueRespondioMayorA20(nombre,usuModerador);
		usuSimple = Mockito.mock(UsuarioSimple.class);
	}
		
	@Test
	public void testConstructor() {
		
		assertEquals(nombre,insignia.getNombre());
		
	}
	
	@Test
	public void testCumpleCondicionDadoUnUsuarioSimpleQueLaCumple(){
		
		Mockito.when(usuSimple.getCantidadDeRespuestas()).thenReturn(21);		
		
		assertTrue(insignia.cumpleCondicion(usuSimple));
	}
	
	@Test
	public void testCumpleCondicionDadoUnUsuarioSimpleQueNoLaCumple(){
		
		Mockito.when(usuSimple.getCantidadDeRespuestas()).thenReturn(10);		
		
		assertFalse(insignia.cumpleCondicion(usuSimple));
	}
	
	@Test
	public void testDetalleParaAlcanzarLaIsigniaCantidadQueRespondioMayorA20DadoUnUsuarioSimpleQueLaCumple(){
		
		Mockito.when(usuSimple.getCantidadDeRespuestas()).thenReturn(21);
		
		assertEquals(insignia.retornarMensajeDetalle(21),insignia.detalleDeInsignia(usuSimple));
		
	}

}