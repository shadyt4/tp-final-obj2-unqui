package insignia;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import usuario.UsuarioModerador;
import usuario.UsuarioSimple;

public class TestInsigniaCompuesta {
	
	InsigniaCompuesta insignia;
	CantidadQueRespondioMayorA20 insignia1;
	CantidadPreguntasMayorA50 insignia2;
	String nombre;
	UsuarioModerador usuModerador;
	UsuarioSimple usuSimple;
	
	@Before
	public void setUp(){
		nombre = "laInsigniaCompleta";
		usuModerador = Mockito.mock(UsuarioModerador.class);
		insignia = new InsigniaCompuesta(nombre,usuModerador);
		insignia1 = new CantidadQueRespondioMayorA20("asd",usuModerador);
		insignia2 = new CantidadPreguntasMayorA50("qwe",usuModerador);
		usuSimple = Mockito.mock(UsuarioSimple.class);
		
		insignia.agregarInsignia(insignia1);
		insignia.agregarInsignia(insignia2);
		
	}
	
	@Test
	public void testConstructor(){
		
		assertEquals(insignia.getNombre(),nombre);
	}

	
	@Test
	public void testCumpleCondicionDadoUnUsuarioSimpleQueCumpleConLasDosInsigniasDeLaCompuesta(){
		
		Mockito.when(usuSimple.getCantidadDePreguntas()).thenReturn(55);
		Mockito.when(usuSimple.getCantidadDeRespuestas()).thenReturn(21);
		
		assertTrue(insignia.cumpleCondicion(usuSimple));
		
	}
	
	@Test
	public void testCumpleCondicionDadoUnUsuarioSimpleQueNoCumpleConLasDosInsigniasDeLaCompuesta(){
	
		Mockito.when(usuSimple.getCantidadDePreguntas()).thenReturn(55);
		Mockito.when(usuSimple.getCantidadDeRespuestas()).thenReturn(14);
		
		assertFalse(insignia.cumpleCondicion(usuSimple));
		
	}
	
	@Test
	public void testDadaUnUsuarioSimpleYUnaInsigniaCompuestaQueLaCumpleRetornaDetalle(){
		
		String stringComparar;
		
		Mockito.when(usuSimple.getCantidadDePreguntas()).thenReturn(55);
		Mockito.when(usuSimple.getCantidadDeRespuestas()).thenReturn(21);
		
		stringComparar = insignia1.detalleDeInsignia(usuSimple) +"// " +insignia2.detalleDeInsignia(usuSimple) +"// ";

		assertEquals(stringComparar,insignia.detalleDeInsignia(usuSimple));
		
	}
	
}
