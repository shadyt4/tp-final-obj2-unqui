package insignia;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import post.Pregunta;
import usuario.UsuarioModerador;
import usuario.UsuarioSimple;

public class TestMasDe5PreguntasEnUnaSemana {

	MasDe5PreguntasEnUnaSemana insignia;
	String nombre;
	UsuarioModerador usuModerador;
	UsuarioSimple usuSimple;
	Pregunta preg1,preg2,preg3,preg4,preg5,preg6;
	DateTime fechaHoy;
	ArrayList<Pregunta> listaPreguntas;
	
	@Before
	public void setUp() {
		
		usuModerador = Mockito.mock(UsuarioModerador.class);
		usuSimple = Mockito.mock(UsuarioSimple.class);
		nombre = "insigniaMasde5preguntasEnUnaSemana";
		insignia = new MasDe5PreguntasEnUnaSemana(nombre,usuModerador);
		preg1 = Mockito.mock(Pregunta.class);
		preg2 = Mockito.mock(Pregunta.class);
		preg3 = Mockito.mock(Pregunta.class);
		preg4 = Mockito.mock(Pregunta.class);
		preg5 = Mockito.mock(Pregunta.class);
		preg6 = Mockito.mock(Pregunta.class);
		fechaHoy = new DateTime();
		listaPreguntas = new ArrayList<Pregunta>();
		listaPreguntas.add(preg1);
		listaPreguntas.add(preg2);
		listaPreguntas.add(preg3);
		listaPreguntas.add(preg4);
		listaPreguntas.add(preg5);
		listaPreguntas.add(preg6);
		
		
	}
	
	@Test
	public void testConstructor() {
		
		assertEquals(insignia.getNombre(),nombre);
	}
	
	@Test
	public void testCunpleCondicionMasDe5PreguntasEnUnaSemanaDadoUnUsuarioQueLascumple(){
		Mockito.when(preg1.getFechaDePublicacion()).thenReturn(fechaHoy.minusDays(5));
		Mockito.when(preg2.getFechaDePublicacion()).thenReturn(fechaHoy.minusDays(3));
		Mockito.when(preg3.getFechaDePublicacion()).thenReturn(fechaHoy.minusDays(2));
		Mockito.when(preg4.getFechaDePublicacion()).thenReturn(fechaHoy.minusDays(1));
		Mockito.when(preg5.getFechaDePublicacion()).thenReturn(fechaHoy.minusDays(4));
		Mockito.when(preg6.getFechaDePublicacion()).thenReturn(fechaHoy.minusDays(1));
		
		Mockito.when(usuSimple.getPreguntasPublicadas()).thenReturn(listaPreguntas);
		
		
		assertTrue(insignia.cumpleCondicion(usuSimple));
	}
	
	@Test
	public void testCunpleCondicionMasDe5PreguntasEnUnaSemanaDadoUnUsuarioQueNOLascumple(){
		
		Mockito.when(preg1.getFechaDePublicacion()).thenReturn(fechaHoy.minusDays(8));
		Mockito.when(preg2.getFechaDePublicacion()).thenReturn(fechaHoy.minusDays(9));
		Mockito.when(preg3.getFechaDePublicacion()).thenReturn(fechaHoy.minusDays(10));
		Mockito.when(preg4.getFechaDePublicacion()).thenReturn(fechaHoy.minusDays(20));
		Mockito.when(preg5.getFechaDePublicacion()).thenReturn(fechaHoy.minusDays(4));	
		Mockito.when(preg6.getFechaDePublicacion()).thenReturn(fechaHoy.minusDays(4));
		
		Mockito.when(usuSimple.getPreguntasPublicadas()).thenReturn(listaPreguntas);
		
		assertFalse(insignia.cumpleCondicion(usuSimple));
	}
	
	@Test
	public void testDadaUnUsuarioSimpleYUnaInsigniaMasDe5PreguntasEnUnaSemanaQueLaCumpleRetornaDetalle(){
		
		Mockito.when(preg1.getFechaDePublicacion()).thenReturn(fechaHoy.minusDays(5));
		Mockito.when(preg2.getFechaDePublicacion()).thenReturn(fechaHoy.minusDays(3));
		Mockito.when(preg3.getFechaDePublicacion()).thenReturn(fechaHoy.minusDays(2));
		Mockito.when(preg4.getFechaDePublicacion()).thenReturn(fechaHoy.minusDays(1));
		Mockito.when(preg5.getFechaDePublicacion()).thenReturn(fechaHoy.minusDays(4));
		Mockito.when(preg6.getFechaDePublicacion()).thenReturn(fechaHoy.minusDays(1));
		
		Mockito.when(usuSimple.getPreguntasPublicadas()).thenReturn(listaPreguntas);

		assertEquals(insignia.retornarMensajeDetalle(6),insignia.detalleDeInsignia(usuSimple));
		
	}
}
