package insignia;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import usuario.*;

public class TestCantidadPreguntasMayorA50 {

	private UsuarioModerador usuModerador;
	private String nombre;
	private CantidadPreguntasMayorA50 insignia;
	private UsuarioSimple usuSimple;
	
	@Before
	public void setUp(){
		
		usuModerador = Mockito.mock(UsuarioModerador.class);
		nombre = "nombreInsignia";
		insignia = new CantidadPreguntasMayorA50(nombre,usuModerador);
		usuSimple = Mockito.mock(UsuarioSimple.class);
	}
		
	@Test
	public void testConstructor() {
		
		assertEquals(insignia.getNombre(),nombre);
		
	}
	
	@Test
	public void testCumpleCondicionDadoUnUsuarioSimpleQueLaCumple(){
		
		Mockito.when(usuSimple.getCantidadDePreguntas()).thenReturn(55);		
		
		assertTrue(insignia.cumpleCondicion(usuSimple));
	}
	
	@Test
	public void testCumpleCondicionDadoUnUsuarioSimpleQueNoLaCumple(){
		
		Mockito.when(usuSimple.getCantidadDeRespuestas()).thenReturn(45);		
		
		assertFalse(insignia.cumpleCondicion(usuSimple));
	}
	
	@Test
	public void testDetalleParaAlcanzarLaIsigniaCantidadPreguntasMayorA50DadoUnUsuarioSimpleQueLaCumple(){
		
		Mockito.when(usuSimple.getCantidadDePreguntas()).thenReturn(55);
		
		assertEquals(insignia.retornarMensajeDetalle(55),insignia.detalleDeInsignia(usuSimple));
		
	}
	
}
