package usuario;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
//import org.mockito.Mockito;

import org.mockito.Mockito;

import post.Pregunta;
import stackOverflow.StackOverflow;

public class TestUsuarioSimple {

	StackOverflow stackOverflow;
	UsuarioSimple uSimple;
	Pregunta miPreguntaMock;
	Pregunta pregunta;
	
	int puntaje;
	float factor;
	
	@Before
	public void setup(){
		this.stackOverflow = Mockito.mock(StackOverflow.class);
		this.uSimple = new UsuarioSimple("Emmanuel", "emmanuel@gmail.com", "usuario2014");
		this.uSimple.stackOverflow = this.stackOverflow;
		
		miPreguntaMock = Mockito.mock(Pregunta.class);
		pregunta = new Pregunta("titulo", "cuerpo", uSimple);
	}
	
	@Test
	public void testConstructor() {		
		assertTrue(this.uSimple.getNombreDeUsuario() == "usuario2014" );
		assertTrue(this.uSimple.getNombre() == "Emmanuel" );
		assertTrue(this.uSimple.getEmail() == "emmanuel@gmail.com" );
		assertTrue(this.uSimple.getPreguntasPublicadas().size() == 0);
		assertTrue(this.uSimple.getPreguntasRespondidas().size() == 0);
	}
	
	@Test
	public void testRealizarUnaPregunta() {
		this.uSimple.realizarPregunta("titulo", "cuerpo");
		
		assertTrue( this.uSimple.getPreguntasPublicadas().size() == 1 );
		assertEquals(1, this.uSimple.getCantidadDePreguntas());
	}
	
	@Test
	public void testRealizarUnaRespuesta() {
		this.uSimple.realizarPregunta("titulo", "cuerpo");
		this.uSimple.realizarRespuesta(miPreguntaMock, "respuesta");
		
		assertTrue( this.uSimple.getPreguntasRespondidas().size() == 1);
	}
	
	@Test
	public void testMarcarComoFavorito(){
		Mockito.when(miPreguntaMock.cantidadDeFavoritos()).thenReturn(1);
		
		this.uSimple.marcarComoFavorito(miPreguntaMock);
		assertEquals(1, miPreguntaMock.cantidadDeFavoritos());
	}
	
	@Test
	public void testVotarPositivo(){		
		this.uSimple.votarPositivo(pregunta);
		assertEquals(1, pregunta.cantidadDeVotos());
	}
	
	@Test
	public void testVotarNegativo(){
		this.uSimple.votarNegativo(pregunta);
		assertEquals(0, pregunta.cantidadDeVotos());
	}
	
	@Test
	public void testCantidadDeRespuestas(){
		this.uSimple.realizarRespuesta(pregunta, "cuerpo");
		
		assertEquals(1, this.uSimple.getCantidadDeRespuestas());
	}
	
	@Test
	public void testPuntajePorActividad(){
		this.uSimple.realizarPregunta("titulo", "cuerpo");
		this.uSimple.realizarRespuesta(miPreguntaMock, "cuerpo");
		
		puntaje = this.uSimple.calcularPuntajePorActividad();
		
		assertTrue(puntaje == 15);
		
	}
	
	@Test
	public void testCalcularFactorDeRegularidadNoSiendolo(){
		
		factor = this.uSimple.calcularFactorDeRegularidad();
		
		assertTrue(factor == 0.75f);
	}
	
	@Test
	public void testCalcularFactorDeRegularidadSiendoRegularPreguntando(){
		this.uSimple.realizarPregunta("titulo", "cuerpo");
		
		factor = this.uSimple.calcularFactorDeRegularidad();
		
		assertTrue(factor == 1);
	}
	
	@Test
	public void testCalcularFactorDeRegularidadSiendoRegularRespondiendo(){
		this.uSimple.realizarRespuesta(pregunta, "cuerpo");
		
		factor = this.uSimple.calcularFactorDeRegularidad();
		
		assertTrue(factor == 1);
	}

	@Test
	public void testCacularRanking(){
		this.uSimple.realizarPregunta("titulo", "cuerpo");
		this.uSimple.realizarRespuesta(miPreguntaMock, "cuerpo");
		
		assertEquals(15f, this.uSimple.calcularRanking(), 0.1f);
	}
	
}
