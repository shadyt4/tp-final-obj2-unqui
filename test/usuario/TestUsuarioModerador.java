package usuario;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import post.Pregunta;
import post.Respuesta;
import stackOverflow.StackOverflow;

public class TestUsuarioModerador {

	UsuarioModerador uModerador;
	UsuarioSimple uSimple;
	Pregunta miPregunta;
	Respuesta miRespuesta;
	float factor;
	
	UsuarioSimple usuario;
	StackOverflow miStack;
	Pregunta preguntaParaEliminar;
	Respuesta respuestaParaEliminar;
	
	@Before
	public void setup() {
		this.uModerador = new UsuarioModerador("Nicolas", "nico.allip@gmail.com");
		this.uSimple = Mockito.mock(UsuarioSimple.class);
		
		usuario = new UsuarioSimple("Emmanuel", "emmanuel@gmail.com", "usuario2014");
		
		miPregunta = new Pregunta("titulo", "cuerpo", this.usuario);
		miRespuesta = new Respuesta("cuerpo", uSimple);
		
		miStack = new StackOverflow();
		usuario.setStackOverflow(miStack);
	}
	
	@Test
	public void testConstructor() {
		
		assertTrue(this.uModerador.getNombre().equals("Nicolas"));
		assertTrue(this.uModerador.getEmail().equals("nico.allip@gmail.com"));
	}
	
	@Test
	public void testModificarPregunta(){		
		this.uModerador.modificarPregunta(miPregunta, "Titulo", "tituloNuevo", this.uModerador);
		assertTrue(miPregunta.getTitulo().equals("tituloNuevo"));
		this.uModerador.modificarPregunta(miPregunta, "Cuerpo", "cuerpoNuevo", this.uModerador);
		assertTrue(miPregunta.getCuerpo().equals("cuerpoNuevo"));
	}
	
	@Test
	public void testModificarRespuesta(){
		
		this.uModerador.modificarRespuesta(miRespuesta, "Texto Modificado", this.uModerador);
		assertTrue(miRespuesta.getCuerpo().equals("Texto Modificado"));
	}
	
	@Test
	public void testPuntajePorActividad(){
		Pregunta p = new Pregunta("titulo", "cuerpo", this.uModerador);
		
		this.uModerador.modificarPregunta(p, "Titulo", "titulo modificado", this.uModerador);
		int puntaje = this.uModerador.calcularPuntajePorActividad();
		
		assertTrue(puntaje == 10);
		
	}
	
	@Test
	public void testCalcularFactorDeRegularidadNoSiendolo(){
		
		float factor = this.uModerador.calcularFactorDeRegularidad();
		
		assertTrue(factor == 0.75f);
	}
	
	@Test
	public void testCalcularFactorDeRegularidadSiendoRegularModificandoPreguntas(){
		
		this.uModerador.modificarPregunta(miPregunta, "Cuerpo", "cuerpo Modificado", this.uModerador);
		
		factor = this.uModerador.calcularFactorDeRegularidad();
		
		assertTrue(factor == 1);
	}
	
	@Test
	public void testCalcularFactorDeRegularidadSiendoRegularModificandoRespuestas(){
		
		this.uModerador.modificarRespuesta(miRespuesta, "cuerpo nuevo" , this.uModerador);
		
		factor = this.uModerador.calcularFactorDeRegularidad();
		
		assertTrue(factor == 1);
	}
	
	@Test
	public void testEliminarPregunta(){
		usuario.realizarPregunta("titulo", "cuerpo");
		
		preguntaParaEliminar = usuario.getPreguntasPublicadas().get(0);
		
		assertTrue(usuario.getPreguntasPublicadas().contains(preguntaParaEliminar));
		uModerador.eliminarPregunta(preguntaParaEliminar);
		assertFalse( usuario.getPreguntasPublicadas().contains(preguntaParaEliminar));
	}
	
	@Test
	public void testEliminarRespuesta(){
		usuario.realizarRespuesta(miPregunta, "cuerpo");

		respuestaParaEliminar = miPregunta.getRespuestas().get(0);
		
		assertEquals(1, usuario.getPreguntasRespondidas().size());
		uModerador.eliminarRespuesta(respuestaParaEliminar);
		assertEquals( 0, usuario.getPreguntasRespondidas().size());
	}

	@Test
	public void testCrearInsignia(){
		
	}
}
