package usuario;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import post.Pregunta;

public class TestUsuarioVisitante {
	
	UsuarioVisitante uVisitante;
	UsuarioSimple uSimple;
	
	Pregunta pregunta;
	Pregunta miPreguntaMock;

	@Before
	public void setup() {
		this.uVisitante = new UsuarioVisitante(22);
		this.uSimple = Mockito.mock(UsuarioSimple.class);
		
		pregunta = new Pregunta("titulo", "cuerpo", this.uSimple);
		miPreguntaMock = Mockito.mock(Pregunta.class);
	}
	
	@Test
	public void testConstructor() {
		assertEquals(22, this.uVisitante.getDireccionIP());
	}
	
	@Test
	public void testRealizarRespuesta(){
		this.uVisitante.realizarRespuesta(miPreguntaMock, "cuerpo");
		
		assertEquals(1, this.uVisitante.getCantidadDeRespuestas());
		assertEquals(1, this.uVisitante.getPreguntasRespondidas().size());
	}
	
	@Test
	public void testEsRegularRespondiendo(){
		this.uVisitante.realizarRespuesta(pregunta, "cuerpo");
		
		assertTrue(this.uVisitante.esRegularRespondiendo());
	}
	
	@Test
	public void testCalcularFactorDeRegularidad(){
		this.uVisitante.realizarRespuesta(pregunta, "cuerpo");
		
		assertEquals(1, this.uVisitante.calcularFactorDeRegularidad(), 0.1f);
		
	}
	
	@Test
	public void testCalcularFactorDeRegularidadSinSerRegular(){
		
		assertEquals(0.75f, this.uVisitante.calcularFactorDeRegularidad(), 0.1f);
	}

	@Test
	public void testCalcularPuntajePorActividad(){
		this.uVisitante.realizarRespuesta(miPreguntaMock, "cuerpo");
		
		assertEquals(2, this.uVisitante.calcularPuntajePorActividad());
	}
	
	@Test
	public void testCalcularRanking(){
		this.uVisitante.realizarRespuesta(pregunta, "cuerpo");
		
		assertEquals(2, this.uVisitante.calcularRanking(), 0.1f);
	}
}
