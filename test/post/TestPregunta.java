package post;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import usuario.UsuarioSimple;

public class TestPregunta {
	
	UsuarioSimple usuarioMock;
	UsuarioSimple uSimple;
	Pregunta pregunta;
	
	@Before
	public void setup() {
		this.usuarioMock = Mockito.mock(UsuarioSimple.class);
		this.uSimple = new UsuarioSimple("nombre", "email", "nombreDeUsuario");
		this.pregunta = new Pregunta("titulo","cuerpo", usuarioMock);
	}
	
	@Test
	public void testConstructor() {			
		assertEquals("titulo", this.pregunta.getTitulo());
		assertEquals("cuerpo", this.pregunta.getCuerpo());
		
		assertEquals(this.usuarioMock, this.pregunta.getAutor());
		
		this.pregunta.addEtiqueta("objetos2");
		this.pregunta.addEtiqueta("tp");
		
		assertEquals(2, this.pregunta.getCantidadDeEtiquetas());
		assertEquals(0 , this.pregunta.getFavoritos().size() );
		assertEquals(0, this.pregunta.getModeradores().size());
		assertEquals(0, this.pregunta.getRespuestas().size());
		assertEquals(0, this.pregunta.getVotos().size());
		
		assertEquals(this.pregunta, this.pregunta);
	}
	
	@Test
	public void testResponder() {
		this.pregunta.responder("cuerpo de respuesta", usuarioMock);
		
		assertEquals(1, this.pregunta.getRespuestas().size());

	}
	
	@Test
	public void testVotarPositivoYNegativo(){
		this.pregunta.votarPositivo(usuarioMock);
		assertTrue(this.pregunta.getVotos().containsKey(this.usuarioMock));
		assertEquals(true, this.pregunta.getVotos().get(this.usuarioMock));
		
		this.pregunta.votarNegativo(usuarioMock);
		assertTrue(this.pregunta.getVotos().containsKey(this.usuarioMock));
		assertEquals(false, this.pregunta.getVotos().get(this.usuarioMock));
		
	}
	
	@Test
	public void testCantidadDeVotos(){
		this.pregunta.votarPositivo(usuarioMock);
		assertEquals(1, this.pregunta.cantidadDeVotos());
		
		this.pregunta.votarNegativo(usuarioMock);
		assertEquals(0, this.pregunta.cantidadDeVotos());
		
	}
	
	@Test
	public void testFavoritos(){
		uSimple.marcarComoFavorito(this.pregunta);
		
		assertEquals(1, this.pregunta.cantidadDeFavoritos());
	}
	
	
	@Test
	public void testCantidadDeRespuestas(){
		uSimple.realizarRespuesta(this.pregunta, "cuerpo");
		
		assertEquals(1, this.pregunta.getCantidadDeRespuestas());
	}
	
}
