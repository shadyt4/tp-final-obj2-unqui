package post;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import usuario.UsuarioRegistrado;

public class TestRespuesta {
	
	UsuarioRegistrado usuarioMock;
	Respuesta respuesta;
	
	@Before
	public void setup() {
		this.usuarioMock = Mockito.mock(UsuarioRegistrado.class);
		this.respuesta = new Respuesta("cuerpo", usuarioMock);
	}
	
	@Test
	public void testConstructorRespuesta() {
		assertEquals("cuerpo", this.respuesta.getCuerpo());
	}

}
