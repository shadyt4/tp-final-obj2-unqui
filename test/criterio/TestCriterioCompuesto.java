package criterio;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import post.Pregunta;

public class TestCriterioCompuesto {

	
	Pregunta pregunta;
	Titulo titulo;
	Etiqueta etiqueta;
	CriterioCompuesto compuesto; 
	@Before
	public void setUp(){
		
		pregunta = Mockito.mock (Pregunta.class);
		titulo = Mockito.mock(Titulo.class);
		etiqueta = Mockito.mock(Etiqueta.class);
		
		Mockito.when(titulo.cumpleCondicion(pregunta)).thenReturn(true);
		Mockito.when(etiqueta.getContenido()).thenReturn("etiqueta");
		
		compuesto = new CriterioCompuesto();
		compuesto.agregarCriterio(titulo);
		
	}
	@Test
	
	public void testCriterioCompuesto() {
						
		assertTrue(compuesto.cumpleCondicion(pregunta));	
		assertTrue(compuesto.getCantidadDeCriterios() == 1);
		
		compuesto.agregarCriterio(etiqueta);
		
		assertTrue(compuesto.getCantidadDeCriterios() == 2);
		
	}

}
