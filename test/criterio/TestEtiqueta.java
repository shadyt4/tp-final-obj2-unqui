package criterio;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;


import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import post.Pregunta;


public class TestEtiqueta {

	Etiqueta etiqueta;
	Pregunta pregunta;
	
	@Before
	public void setUp(){
		
		etiqueta = new Etiqueta ("etiqueta");
	    pregunta = Mockito.mock(Pregunta.class);
	    
	    Mockito.when(pregunta.getCantidadDeEtiquetas()).thenReturn(0);
	    
		
	}
	@Test
    
    public void testEtiqueta() {
        
	    assertFalse(etiqueta.cumpleCondicion(pregunta));
	    assertTrue(etiqueta.getContenido() == "etiqueta");
	    
    }
}