package criterio;

import static org.junit.Assert.*;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import post.Pregunta;

public class TestFechaDeRealizacion {

	
	Pregunta pregunta;
	DateTime contenido2;
	FechaDeRealizacion fechaRealizacion;
	DateTime contenido3;
	FechaDeRealizacion fechaRealizacion2;
	
	@Before
	public void setUp(){
		pregunta = Mockito.mock(Pregunta.class);
		contenido2 = new DateTime();
		fechaRealizacion = new FechaDeRealizacion(contenido2);
		
	    
	    Mockito.when(pregunta.getFechaDePublicacion()).thenReturn(contenido2);
	    
	    contenido3 = new DateTime();
		fechaRealizacion2 = new FechaDeRealizacion(contenido3);
		
		Mockito.when(pregunta.getFechaDePublicacion()).thenReturn(contenido2);
	}
	@Test
	public void testFechaDeREalizacion() {
	
	assertTrue(fechaRealizacion.cumpleCondicion(pregunta));
	assertFalse(fechaRealizacion2.cumpleCondicion(pregunta));
	
	}
	
}
