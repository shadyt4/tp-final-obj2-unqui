package criterio;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import post.Pregunta;
import usuario.UsuarioRegistrado;
import busqueda.Busqueda;

public class TestBusqueda {

	ArrayList<Criterio> listaDeCriterios;
	Busqueda busqueda;
	Titulo titulo;
	ArrayList<Pregunta> listaDePreguntas;
	Pregunta pregunta;
	UsuarioRegistrado usuarioR;
	
	@Before
	public void setUp(){
	
		pregunta = new Pregunta("titulo", "cuerpo", usuarioR);
		listaDePreguntas = new ArrayList<Pregunta>();
		listaDeCriterios = new ArrayList<Criterio>();
		busqueda = new Busqueda(listaDeCriterios);
		
		titulo = new Titulo("titulo");
		this.listaDeCriterios.add(titulo);
		this.listaDePreguntas.add(pregunta);
	}
	
	@Test
	public void testBusqueda() {
		
		assertTrue(busqueda.realizarBusqueda(listaDePreguntas).size() == 1);
	}

}
