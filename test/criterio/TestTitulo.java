package criterio;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import post.Pregunta;
import usuario.UsuarioSimple;

public class TestTitulo {

	UsuarioSimple usuarioSimple;
	Titulo titulo;
	Pregunta pregunta;
	
	@Before
	public void setUp(){
	
		usuarioSimple = new UsuarioSimple("nicolas","@hotmail","nico");
	    titulo = new Titulo("titulo");
	    pregunta = Mockito.mock(Pregunta.class);
	    
	    Mockito.when(pregunta.getTitulo()).thenReturn("titulo");
	    
	}
	@Test
	public void testTitulo() {
    
	assertTrue(titulo.cumpleCondicion(pregunta));
    assertEquals(pregunta.getTitulo(),titulo.getContenido());
	
	}
    
}
