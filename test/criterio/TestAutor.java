package criterio;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import post.Pregunta;
import usuario.UsuarioModerador;

public class TestAutor {

	Autor autor;
	Pregunta pregunta;
	UsuarioModerador uModerador;
	
	@Before
	public void setUp(){
		
		autor = new Autor("Pedro");
		pregunta = Mockito.mock(Pregunta.class);
		uModerador = new UsuarioModerador("Pedro","@hotmail.com") ;
		
		
		Mockito.when(pregunta.getAutor()).thenReturn(uModerador);
		Mockito.when(pregunta.dameNombreDeAutor()).thenReturn("Pedro");
	}
	@Test
	public void testAutor() {

		assertTrue(autor.cumpleCondicion(pregunta));
	}

}
