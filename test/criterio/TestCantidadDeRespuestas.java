package criterio;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import post.Pregunta;
import post.Respuesta;
import usuario.UsuarioSimple;



public class TestCantidadDeRespuestas {

	CantidadDeRespuestas cantidadDeRespuestas;
	Respuesta respuesta;
	UsuarioSimple usuarioSimple;
	Pregunta pregunta;
	
	@Before
	public void setUp(){
		
		cantidadDeRespuestas = new CantidadDeRespuestas (0);
	    respuesta = new Respuesta("hola");
	    usuarioSimple = new UsuarioSimple("nicolas","@hotmail","nico");
	    pregunta = Mockito.mock(Pregunta.class);
	  
	    Mockito.when(pregunta.getCantidadDeRespuestas()).thenReturn(0);
	    
	}
	 @Test
		public void testCantidadDeRespuestas() {
		    
    assertTrue(cantidadDeRespuestas.cumpleCondicion(pregunta));
	 }
}