package busqueda;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import post.Pregunta;
import criterio.Criterio;
import criterio.Titulo;

public class TestBusqueda {

	List<Criterio> listaCriterios;
	List<Pregunta> listaPreguntas;
	Titulo titulo;
	Pregunta pregunta1;
	Pregunta pregunta2;
	Busqueda busqueda;
	
	@Before
	public void setUp(){
		
		titulo = Mockito.mock(Titulo.class);
		pregunta1 = Mockito.mock(Pregunta.class);
		pregunta2 = Mockito.mock(Pregunta.class);
		
		listaCriterios = new ArrayList<Criterio>();
		listaPreguntas = new ArrayList<Pregunta>();
		
		Mockito.when(titulo.cumpleCondicion(pregunta1)).thenReturn(true);
		Mockito.when(titulo.cumpleCondicion(pregunta2)).thenReturn(false);
		
		listaPreguntas.add(pregunta1);
		listaPreguntas.add(pregunta2);
		listaCriterios.add(titulo);
		
		busqueda = new Busqueda(listaCriterios);
	}
	@Test
	public void test() {
		
		assertEquals(1, busqueda.realizarBusqueda(listaPreguntas).size());
	}

}
