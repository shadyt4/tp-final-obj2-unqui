package stackOverflow;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import post.Pregunta;
import usuario.UsuarioModerador;
import usuario.UsuarioSimple;
import usuario.UsuarioVisitante;
import criterio.Criterio;
import criterio.Titulo;

public class TestStackOverflow {

	private StackOverflow stackOverflow;
	private Titulo titulo;
	private Pregunta pregunta1;
	private Pregunta pregunta2;
	private List<Criterio> listaCriterios;
	private UsuarioModerador usuarioM;
	private UsuarioSimple usuarioS;
	private UsuarioVisitante usuarioV;
	
	@Before
	public void setUp(){
		stackOverflow = new StackOverflow();
		titulo = Mockito.mock(Titulo.class);
		pregunta1 = Mockito.mock(Pregunta.class);
		pregunta2 = Mockito.mock(Pregunta.class);
		
		stackOverflow.listaDePreguntas.add(pregunta1);
		stackOverflow.listaDePreguntas.add(pregunta2);
		listaCriterios = new ArrayList<Criterio>();
		
		listaCriterios.add(titulo);
		
		Mockito.when(titulo.cumpleCondicion(pregunta1)).thenReturn(true);
		Mockito.when(titulo.cumpleCondicion(pregunta2)).thenReturn(false);
		
	}
	
	@Test
	public void testBuscarStackOverflow() {
		
		assertTrue(stackOverflow.buscar(listaCriterios).size() == 1);
		
	}

	@Test
	public void	testAgregarUsuarioModerador(){
		
		usuarioM = new UsuarioModerador("pedro", "@gmail.com");
		
		stackOverflow.listaDeUsuariosModeradores.add(usuarioM);
		assertTrue(stackOverflow.listaDeUsuariosModeradores.size() == 1);
		
	}
	
	@Test
	public void	testAgregarUsuarioSimple(){
		
		usuarioS = new UsuarioSimple("pedro", "@gmail.com","pepe");
		
		stackOverflow.listaDeUsuariosSimple.add(usuarioS);
		assertTrue(stackOverflow.listaDeUsuariosSimple.size() == 1);
	}
	
	
	@Test
	public void	testAgregarUsuarioVisitante(){
		
		usuarioV = new UsuarioVisitante(10910);
		
		stackOverflow.listaDeUsuariosVisitante.add(usuarioV);
		assertTrue(stackOverflow.listaDeUsuariosVisitante.size() == 1);
		
	}
	
	@Test
	public void	testGetPreguntas(){
		
		assertTrue(stackOverflow.listaDePreguntas.size() == 2);
		
	}
	
	@Test
	public void	testBuscarUsuarioSimple(){
		
		assertTrue(stackOverflow.buscarUsuarioSimple("pedro") == usuarioS);
	}
	
	
	@Test
	public void	testEstaUsuarioModerador(){
		
		assertTrue(stackOverflow.buscarYRetornarUsuarioModerador("pedro") == usuarioM);
	}
	
	@Test
	public void buscarUsuarioVisitante(){
	
		assertTrue(stackOverflow.buscarYRetornarUsuarioVisitante(10910) == usuarioV);
	}
}
